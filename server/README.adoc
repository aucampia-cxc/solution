= CoopX Case: Server

This directory contains a npm project for the server part of my solution for the CoopX Case.

This server requires credentials to run which it will read from the following environment variables:

* `TWITTER_API_KEY`
* `TWITTER_API_SECRET_KEY`
* `TWITTER_API_BEARER_TOKEN`

These environment variables should be set to values supplied independently outside of this repo.

These values can also be placed in a `.env` file which will be loaded when the server starts up.

An example `.env` file can be seen in `example.env`.

To get it running you will need to do the following

[source, bash]
----
yarn install
## make sure .env has the correct values or set the required environment variables.
yarn start
----

It should also work with npm.

== Configuration

The behaviour of the server can be controlled with the following envionment variables:

`IACXC_SERVER_FILTERS`:: This environment variable should be a json array of strings which will be set as the filters on the twitter stream. If this value is not set or is an empty array then the sampled stream will be used.
example::: ``IACXC_SERVER_FILTERS=["cars"]``
default::: ``IACXC_SERVER_FILTERS=[]`` (i.e. use sampled stream)

`IACXC_SERVER_RECONNECT_DELAY`:: This environment variable sets the amount of time that the server will wait before reconnecting to the twitter stream if it disconnects.
default::: ``IACXC_SERVER_RECONNECT_DELAY=5000``

`IACXC_RECONNECT_ON_ERROR`:: This environment variable determines whether or not the server will reconnect to twitter if an error occurs.
default::: ``IACXC_SERVER_RECONNECT_DELAY=yes``
valid values::: ``yes`` or ``no``

