/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-non-null-assertion */
import consola from "consola";
const console = undefined; // eslint-disable-line @typescript-eslint/no-unused-vars

import debugm from "debug";
import http from "http";
import WebSocket from "ws";
import url from "url";
import createExpressApp from "./expressApp";
import constants from "./constants";
import net from "net";
import { DateTime } from "luxon";
import TwitterClient from "./TwitterClient";
import { TwitterStream } from "twitter-v2/src/TwitterStream";
import { CloudEvent } from "cloudevents";
import { RequestParameters } from "twitter-v2";
import { CredentialsArgs } from "twitter-v2/src/Credentials";

const debug = debugm("iacxs:server:actual");
const debugDump = debugm("iacxs:server:dump");

interface NodeWebSocket extends WebSocket {
  _socket: net.Socket;
}

function onListening(server: http.Server) {
  const addr = server.address();
  const bind = typeof addr === "string" ? "pipe " + addr : "port " + addr!.port;
  debug("Listening on " + bind);
}

function onError(port: string | number, error: NodeJS.ErrnoException) {
  if (error.syscall !== "listen") {
    consola.error(`error = `, error);
    throw error;
  }

  const bind = typeof port === "string" ? "Pipe " + port : "Port " + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case "EACCES":
      consola.error(bind + " requires elevated privileges");
      process.exit(1);
      break;
    case "EADDRINUSE":
      consola.error(bind + " is already in use");
      process.exit(1);
      break;
    default:
      throw error;
  }
}

export interface ServerOptions {
  port: number;
  filters: string[];
  passEvery: number | null;
  reconnectDelay: number | null;
  reconnectOnError: boolean;
  twitterCredentials: CredentialsArgs;
}

function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

export class Server {
  options: ServerOptions;
  shutDown: boolean;
  constructor(options: ServerOptions) {
    consola.info({ ...options, twitterCredentials: "*masked*" });
    this.options = options;
    this.shutDown = false;
  }
  async run(): Promise<void> {
    const {
      port,
      filters,
      passEvery,
      reconnectDelay,
      reconnectOnError,
      twitterCredentials,
    } = this.options;
    debug({ port, filters });
    const expressApp = await createExpressApp();
    expressApp.set("port", this.options.port);
    // const server = http.createServer(expressApp);
    // server.listen(port);
    const server = expressApp.listen(port);
    const wss = new WebSocket.Server({ noServer: true });
    let twitterStream: TwitterStream<Record<string, any>> | null = null;
    server.on("error", (error) => onError(port, error));
    server.on("listening", () => onListening(server));

    process.on("exit", (code: number) => {
      consola.info(`exit handler: ${code}`);
    });
    const signalHandler = (signal: NodeJS.Signals) => {
      consola.info(`signal handler: ${signal}`);
      // if (this.shutDown) return;
      this.shutDown = true;
      if (twitterStream != null) {
        consola.info(`Closing twitterStream ...`);
        twitterStream.close();
      }
      consola.info(`closing wss ...`);
      wss.close((error) => {
        if (error) consola.log(error);
        consola.info(`wss.close(): done`);
        consola.info(`closing server ...`);
        server.close((error) => {
          if (error) consola.log(error);
          consola.info(`server.close(): done`);
          process.exit(0);
        });
      });
    };
    const signals: NodeJS.Signals[] = ["SIGTERM", "SIGINT"];
    for (const signal of signals) {
      process.on(signal, signalHandler);
    }
    process.on("SIGUSR1", () => {
      if (twitterStream != null) {
        twitterStream.close();
      }
    });
    server.on("upgrade", (request, socket, head) => {
      const pathname = url.parse(request.url).pathname;
      if (this.shutDown) {
        socket.write("HTTP/1.1 503 Service Unavailable\r\n\r\n");
        socket.close();
      } else if (pathname === constants.wsPath) {
        wss.handleUpgrade(request, socket, head, function done(ws) {
          wss.emit("connection", ws, request);
        });
      } else {
        socket.write("HTTP/1.1 404 Not Found\r\n\r\n");
        socket.destroy();
      }
    });
    wss.on("connection", function connection(ws) {
      const { remoteAddress, remotePort } = (ws as NodeWebSocket)._socket;
      consola.info(`connected from: ${remoteAddress}:${remotePort}`);
      ws.on("message", (message) => {
        debugDump("received:", message);
      });
      ws.on("close", (ws: WebSocket, code: number, reason: string) => {
        consola.info(
          `disconnected from ${ws} ${remoteAddress}:${remotePort} code=${code} reason=${reason}`
        );
      });
    });

    setInterval(() => {
      for (const client of wss.clients) {
        const heartbeatMessage = new CloudEvent({
          // datacontenttype: "application/json",
          type: "iacxc.v0x0.heartbeat",
          source: "/iacxc/server",
          data: {
            datetime: DateTime.utc().toFormat("yyyy-MM-dd'T'HH:mm:ssZZ"),
          },
        });
        client.send(JSON.stringify(heartbeatMessage));
      }
    }, 1000);

    const twitterClient = new TwitterClient(twitterCredentials);

    const streamParameters: RequestParameters = {
      expansions: "author_id",
      "tweet.fields": "created_at",
    };
    let streamPath;
    if (filters.length > 0) {
      streamPath = "tweets/search/stream";
      await twitterClient.setStreamFilters(filters);
    } else {
      streamPath = "tweets/sample/stream";
    }

    while (!this.shutDown) {
      try {
        twitterStream = twitterClient.stream(streamPath, streamParameters);
        let iter = 0;
        for await (const tweet of twitterStream) {
          if (iter > 1000000) iter = 0;
          if (passEvery == null || iter++ % passEvery === 0) {
            for (const client of wss.clients) {
              debugDump("sending tweet =", tweet);
              const tweetMessage = new CloudEvent({
                type: "iacxc.v0x0.tweet",
                source: "/iacxc/server",
                data: tweet,
              });

              client.send(JSON.stringify(tweetMessage));
            }
          } else {
            // debugDump("dropping tweet =", data);
          }
        }
        consola.info("Stream ended ...");
      } catch (error) {
        consola.error(`error =`, error);
        if (!reconnectOnError) {
          throw error;
        }
      }
      if (!this.shutDown && reconnectDelay != null) {
        consola.info(
          `Sleeping for ${reconnectDelay} ms before reconnecting ...`
        );
        await sleep(reconnectDelay);
      }
    }
  }
}
