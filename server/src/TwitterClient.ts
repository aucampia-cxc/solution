/* eslint-disable @typescript-eslint/no-explicit-any */
import Twitter from "twitter-v2";
// import { TwitterStream } from "twitter-v2/src/TwitterStream";

import debugm from "debug";
const debug = debugm("iacxs:server:TwitterClient");

export default class TwitterClient extends Twitter {
  async setStreamFilters(requestedFilters: Iterable<string>): Promise<void> {
    const requestedFiltersSet = new Set<string>(requestedFilters);
    const filters: any = await this.get("tweets/search/stream/rules");
    const addFilters = new Set<string>();
    const deleteFilterIds = new Set<string>();
    const currentFilters = new Map<string, string>();
    debug(`filters =`, filters);
    for (const filter of filters.data || []) {
      currentFilters.set(filter.value, filter.id);
      if (!requestedFiltersSet.has(filter.value)) {
        deleteFilterIds.add(filter.id);
      }
    }
    for (const filter of requestedFiltersSet) {
      if (!currentFilters.has(filter)) {
        addFilters.add(filter);
      }
    }
    debug(`addFilters = `, addFilters);
    if (deleteFilterIds.size > 0) {
      const ruleUpdate = {
        delete: {
          ids: Array.from(deleteFilterIds.values()),
        },
      };
      debug(`ruleUpdate = `, JSON.stringify(ruleUpdate));
      await this.post("tweets/search/stream/rules", ruleUpdate);
    }
    if (addFilters.size > 0) {
      const ruleUpdate = {
        add: Array.from(addFilters.values()).map((value) => ({
          value,
        })),
      };
      debug(`ruleUpdate = `, JSON.stringify(ruleUpdate));
      await this.post("tweets/search/stream/rules", ruleUpdate);
    }
  }
}
