#!/usr/bin/env node

/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-var-requires */

import pathm from "path";
const __file__ = pathm.basename(__filename);

const debugm = require("debug");
const debug = debugm(`iacxs:server:${__file__}`);
require("debug-trace")({
  patchOutput: false,
  overwriteDebugLog: console.error,
});
import consola from "consola";
import { BasicReporter } from "consola";
(consola as any)._stdout = process.stderr;

import yargs from "yargs";
import * as runCommand from "./commands/run";
import * as twitterDumpCommand from "./commands/twitter-dump";

export async function main(): Promise<void> {
  const middleware = (args: yargs.Arguments<any>) => {
    consola.level += args.verbose;
    debug(`entry:`, { args, level: consola.level });
  };
  const parser = yargs
    .strict()
    .middleware([middleware])
    .command(runCommand)
    .command(twitterDumpCommand)
    .help("h")
    .alias("help", "h")
    .option("verbose", {
      alias: "v",
      describe: "increase verbosity",
      count: true,
      default: 0,
    })
    .showHelpOnFail(false);
  const parsed = parser.argv;
  consola.level += parsed.verbose;
}

main().catch((error) => {
  consola.error(`error =`, error);
  process.exitCode = 1;
});
