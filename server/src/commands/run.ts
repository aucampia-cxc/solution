#!/usr/bin/env node
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-non-null-assertion */

import lodash from "lodash";

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import consola from "consola";
const console = undefined; // eslint-disable-line @typescript-eslint/no-unused-vars

import yargs from "yargs";
import yaml from "yaml";
import dotenv from "dotenv";
import debugm from "debug";
import { CredentialsArgs } from "twitter-v2/src/Credentials";
import { Server } from "../Server";

const debug = debugm("iacxs:server:cli:run");

export const command = "run";
export const describe = "run the server";

export function builder(yargs: yargs.Argv): yargs.Argv {
  yargs
    .option("port", {
      alias: "p",
      describe: "port to listen on",
      type: "string",
      default: null,
    })
    .option("sampled", {
      boolean: true,
      default: null,
      describe: "run against the sampled stream",
    })
    .option("pass-every", {
      hidden: true,
      number: true,
      default: null,
      describe: "pass every nth tweet",
    })
    .option("filter", {
      array: true,
      default: [],
      describe: "run against the sampled stream",
    })
    .option("reconnect-delay", {
      number: true,
      default: null,
      describe: "run against the sampled stream",
    })
    .option("reconnect-on-error", {
      string: true,
      default: undefined,
      choices: ["yes", "no", undefined],
      describe: "run against the sampled stream",
    });
  return yargs;
}

export async function handler(argv: yargs.Arguments): Promise<void> {
  debug("entry ...");
  dotenv.config();
  const portString =
    (argv.port as string | null) ?? process.env.PORT ?? "30733";
  const port = parseInt(portString);
  if (isNaN(port)) {
    throw new Error(`port ${portString} is not a number ...`);
  }
  let filters =
    ((argv.filter as string[]).length ? argv.filter : null) ??
    (lodash.has(process.env, "IACXC_SERVER_FILTERS")
      ? yaml.parse(process.env.IACXC_SERVER_FILTERS!)
      : null) ??
    [];
  if (argv.sampled as boolean) {
    filters = [];
  }
  const reconnectDelayString =
    (argv.reconnectDelay as string | null) ??
    process.env.IACXC_SERVER_RECONNECT_DELAY ??
    "5000";
  const reconnectOnError =
    ((argv.reconnectOnError as string | null)
      ? argv.reconnectOnError === "yes"
      : null) ??
    (process.env.IACXC_RECONNECT_ON_ERROR
      ? process.env.IACXC_RECONNECT_ON_ERROR === "yes"
      : null) ??
    true;
  const reconnectDelay =
    reconnectDelayString != null ? parseInt(reconnectDelayString) : null;

  for (const envkey of [
    "TWITTER_API_BEARER_TOKEN",
    "TWITTER_API_KEY",
    "TWITTER_API_SECRET_KEY",
  ]) {
    if (!lodash.has(process.env, envkey)) {
      throw new Error(`Missing environment variable ${envkey}`);
    }
  }
  const twitterCredentials: CredentialsArgs = {
    bearer_token: process.env.TWITTER_API_BEARER_TOKEN!,
    consumer_key: process.env.TWITTER_API_KEY!,
    consumer_secret: process.env.TWITTER_API_SECRET_KEY!,
  };
  const server = new Server({
    port,
    filters,
    reconnectDelay,
    reconnectOnError,
    twitterCredentials,
    passEvery: argv.passEvery as number | null,
  });
  await server.run();
}
