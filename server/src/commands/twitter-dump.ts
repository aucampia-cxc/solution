#!/usr/bin/env node
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-non-null-assertion */

import consola from "consola";
const console = undefined; // eslint-disable-line @typescript-eslint/no-unused-vars

import lodash from "lodash";

import yargs from "yargs";
import debugm from "debug";
import dotenv from "dotenv";
import { TwitterStream } from "twitter-v2/src/TwitterStream";
import TwitterClient from "../TwitterClient";

const debug = debugm("iacxs:server:cli:twitter-dump");

export const command = "twitter dump";
export const describe = "dump twitter stream";

export function builder(yargs: yargs.Argv): yargs.Argv {
  yargs
    .option("sampled", {
      boolean: true,
      default: null,
      describe: "run against the sampled stream",
    })
    .option("filter", {
      array: true,
      default: [],
      describe: "run against the sampled stream",
    });
  return yargs;
}

export async function handler(argv: yargs.Arguments): Promise<void> {
  dotenv.config();
  for (const envkey of [
    "TWITTER_API_BEARER_TOKEN",
    "TWITTER_API_KEY",
    "TWITTER_API_SECRET_KEY",
  ]) {
    if (!lodash.has(process.env, envkey)) {
      throw new Error(`Missing environment variable ${envkey}`);
    }
  }

  const signalHandler = (signal: NodeJS.Signals) => {
    consola.info(`signal handler: ${signal}`);
    if (twitterStream != null) {
      consola.info(`Closing twitterStream ...`);
      twitterStream.close();
    }
  };
  const signals: NodeJS.Signals[] = ["SIGTERM", "SIGINT"];
  for (const signal of signals) {
    process.on(signal, signalHandler);
  }

  let twitterStream: TwitterStream<Record<string, any>> | null = null;

  const twitterClient = new TwitterClient({
    bearer_token: process.env.TWITTER_API_BEARER_TOKEN,
    consumer_key: process.env.TWITTER_API_KEY!,
    consumer_secret: process.env.TWITTER_API_SECRET_KEY!,
  });
  const requestedFilters = new Set<string>(argv.filter as string[]);
  debug(`requestedFilters = `, requestedFilters);
  try {
    if (argv.sampled === true) {
      twitterStream = twitterClient.stream("tweets/sample/stream");
    } else {
      await twitterClient.setStreamFilters(requestedFilters);
      twitterStream = twitterClient.stream("tweets/search/stream");
    }
    for await (const { data } of twitterStream) {
      consola.log("tweet = %s", data);
    }
  } catch (error) {
    consola.error(`error(${error.code}) =`, error);
    throw error;
  }
}
