const constants = {
  apiPath: "/twitter/v1.0/api/",
  wsPath: "/twitter/v1.0/stream/",
};

export default constants;
