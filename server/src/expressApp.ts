/* eslint-disable @typescript-eslint/no-explicit-any */
import express from "express";
import cookieParser from "cookie-parser";
import morgan from "morgan";
import path from "path";
import { DateTime } from "luxon";

async function createExpressApp(): Promise<express.Express> {
  const expressApp = express();
  (expressApp as any).use(morgan("dev"));
  expressApp.use(express.json());
  expressApp.use(express.urlencoded({ extended: false }));
  expressApp.use(cookieParser());
  expressApp.use(express.static(path.join(__dirname, "public")));

  const twitterRouter = express.Router();
  twitterRouter.get("/ping", (request, response) => {
    response.json({
      pong: DateTime.utc().toFormat("yyyy-MM-dd'T'HH:mm:ssZZ"),
    });
  });
  expressApp.use("/twitter/v1.0/api/", twitterRouter);
  return expressApp;
}

export default createExpressApp;
