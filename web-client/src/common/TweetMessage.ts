export default interface TweetMessage {
  data: { created_at: string; id: string; text: string; author_id: string };
  includes: { users: { id: string; name: string; username: string }[] };
}
