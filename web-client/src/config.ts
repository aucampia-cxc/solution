const url = new URL(window.location.href);

const backendSecure = (url.searchParams.get("secure") || "").length
  ? url.searchParams.get("secure") === "true"
  : false;
const backendHost = url.searchParams.get("backend-host") || "localhost:30733";

function fixUrlScheme(secure: boolean, baseScheme: "http" | "ws") {
  if (secure) {
    return `${baseScheme}s`;
  }
  return baseScheme;
}

const backendHttp =
  url.searchParams.get("backend-http") ||
  `${fixUrlScheme(backendSecure, "http")}://${backendHost}/twitter/v1.0/api/`;
const backendWs =
  url.searchParams.get("backend-ws") ||
  `${fixUrlScheme(backendSecure, "ws")}://${backendHost}/twitter/v1.0/stream/`;

const tweetLimit = 25;

const config = { backendSecure, backendHost, backendHttp, backendWs, tweetLimit };

export default config;
