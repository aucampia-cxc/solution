import { CloudEvent } from "cloudevents";
import useWebSocket, { ReadyState } from "react-use-websocket";
import config from "../config";
import TweetMessage from "../common/TweetMessage";
import React from "react";
import { Alert, Color } from "@material-ui/lab";
import { useSelector } from "react-redux";
import { store, appSlice, RootState } from "../redux";

import { List, ListItem, ListItemText, Grid, Link } from "@material-ui/core";

function Tweets(props: { tweets: TweetMessage[] }) {
  const { tweets } = props;
  return (
    <React.Fragment>
      <List>
        {tweets.map((tweet) => (
          <ListItem key={tweet.data.id}>
            <ListItemText>
              <Link
                href={`https://twitter.com/${tweet.includes.users[0]?.username}/status/${tweet.data.id}`}
              >{`(${tweet.data.created_at})`}</Link>
              {` <@${tweet.includes.users[0]?.username}> : ${tweet.data.text}`}
            </ListItemText>
          </ListItem>
        ))}
      </List>
    </React.Fragment>
  );
}

export default function Main(prop: {}) {
  const { readyState } = useWebSocket(config.backendWs, {
    shouldReconnect: (closeEvent) => true,
    reconnectInterval: 10000,
    reconnectAttempts: Infinity,
    share: true,
    onOpen: () => console.log("ws opened"),
    onClose: () => console.log("ws closed"),
    onMessage: (event) => {
      const cloudEvent = new CloudEvent(JSON.parse(event.data));
      if (cloudEvent.type === "iacxc.v0x0.tweet") {
        store.dispatch(
          appSlice.actions.handleTweet({
            value: cloudEvent.data as TweetMessage,
          })
        );
      } else if (cloudEvent.type !== "iacxc.v0x0.heartbeat") {
        console.log("cloudEvent", cloudEvent);
      }
    },
  });

  const readyStateMap: Record<
    ReadyState,
    {
      severity: Color;
      text: string;
    }
  > = {
    [ReadyState.CONNECTING]: { severity: "warning", text: "connecting ..." },
    [ReadyState.OPEN]: { severity: "success", text: "connected." },
    [ReadyState.CLOSING]: { severity: "error", text: "disconnecting ..." },
    [ReadyState.CLOSED]: { severity: "error", text: "disconnected." },
    [ReadyState.UNINSTANTIATED]: {
      severity: "warning",
      text: "initializing ...",
    },
  };

  const mappedState = readyStateMap[readyState];

  const tweets = useSelector((state: RootState) => state.app.tweets);
  return (
    <React.Fragment>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Grid item xs={12}>
            <Alert severity={mappedState.severity}>{mappedState.text}</Alert>
          </Grid>
        </Grid>
      </Grid>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Tweets tweets={tweets} />
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
