import { createHashHistory } from "history";

import { configureStore, createSlice, combineReducers } from "@reduxjs/toolkit";

import { connectRouter, routerMiddleware } from "connected-react-router";
import TweetMessage from "./common/TweetMessage";
import config from "./config";


export const history = createHashHistory({
  hashType: "slash",
});
export const historyReducer = connectRouter(history);

export const appSlice = createSlice({
  name: "app",
  initialState: {
    tweets: [] as TweetMessage[],
  },
  reducers: {
    handleTweet(
      state,
      action: {
        type: string;
        payload: { value: TweetMessage };
      }
    ) {
      state.tweets.unshift(action.payload.value);
      while (state.tweets.length > config.tweetLimit) {
        state.tweets.pop();
      }
    },
  },
});

export type RootState = ReturnType<typeof rootReducer>;
export type RootAction = Parameters<typeof rootReducer>[1];

export const rootReducer = combineReducers({
  app: appSlice.reducer,
  router: historyReducer,
});

const appMiddleware = (store: any) => (next: any) => (action: RootAction) => {
  // const state = store.getState() as RootState;
  // console.log(`appMiddleware: state.router =`, state.router);
  const result = next(action);
  return result;
};

export const store = configureStore({
  reducer: rootReducer,
  middleware: [appMiddleware, routerMiddleware(history)],
});
